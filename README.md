# the notes of my postgraduate period

#### 介绍
学习笔记

#### 说明
本人学习的方向是机器学习，需要涉及到许多数学知识，在本科许多都没接触，并且许多公式也只是在做题
略知一二，数学知识也未与现在学习的东西对接好，作此笔记也是为了复习知识，串联知识。
现在学校也没开相关数学课程，学习过程中又需要这些相关的先验知识，所以选择自学，
当然笔记会因自己的理解程度而写，有了新的理解也会进行缝缝补补，作此笔记也是以备忘记之时查阅，
同时也熟练去使用latex。

为自己缺乏的知识打补丁，所有pdf文件使用latex排版生成。

内容包括：
+ 总结的latex\tex使用：包含tex文件
+ 离散数学
+ 线性代数
+ 高等数学
+ 凸优化
+ 数值最优化

文件夹结构：<br>
-- the-notes-of-my-postgraduate-period <br>
------ advancedMathmatics <br>
---------- 导数与梯度 <br>
------------- .pdf文件 <br>
------------- .tex文件 <br>
------------- figure(图片文件夹) <br>
------ discreteMath <br>
------ linearAlgebre <br>
------ Optimization <br>
------ probability <br>
------ usageOflatex <br>

#### 目的
了解一些数学来源，为自己的学习夯实(这个词想起了高中)数学基础。
